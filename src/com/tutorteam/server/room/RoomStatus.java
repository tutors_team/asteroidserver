package com.tutorteam.server.room;

public enum RoomStatus {
    WAITING_CONNECTIONS,
    GAMING,
    FINISHED
}
