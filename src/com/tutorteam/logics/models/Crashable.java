package com.tutorteam.logics.models;

/**
 * @author Bulat Giniyatullin
 * 10 Декабрь 2017
 */

public interface Crashable {
    void crash();
}
