package com.tutorteam.logics.models;

import com.tutorteam.logics.Screen;

/**
 * @author Bulat Giniyatullin
 * 08 Декабрь 2017
 */

public interface Model {
    void render(Screen screen);
}
